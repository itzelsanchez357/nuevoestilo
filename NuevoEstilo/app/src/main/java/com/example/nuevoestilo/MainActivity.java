package com.example.nuevoestilo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Casteo de botón
        Button btnRegistro = (Button) findViewById(R.id.btnRegistro);

        // Dar evento a botón
        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent explícito que mostrará el ActivityRegistro
                Intent i = new Intent(v.getContext(), ActivityRegistro.class);
                // Iniciar activity
                startActivity(i);
            }
        });
    }
}