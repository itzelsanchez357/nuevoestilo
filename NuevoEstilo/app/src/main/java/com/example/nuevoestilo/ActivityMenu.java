package com.example.nuevoestilo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import lecho.lib.hellocharts.model.PieChartData;

import lecho.lib.hellocharts.view.PieChartView;
import lecho.lib.hellocharts.model.SliceValue;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class ActivityMenu extends AppCompatActivity {
    PieChartView pieChartView;
    FloatingActionButton fBtnContacto;
    FloatingActionButton fBtnCatalogo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        // Casteo de botones flotantes (floatingActionButton)
        fBtnContacto = (FloatingActionButton) findViewById(R.id.fBtnContacto);
        fBtnCatalogo = (FloatingActionButton) findViewById(R.id.fBtnCatalogo);

        // Dar evento a botón
        fBtnContacto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent explícito que mostrará el ActivityContacto
                Intent i = new Intent(v.getContext(), ActivityContacto.class);
                startActivity(i);
            }
        });
        // Dar evento a botón
        fBtnCatalogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent explícito que mostrará el activityCatalogo
                Intent i = new Intent(v.getContext(), ActivityCatalogo.class);
                startActivity(i);
            }
        });

        // Cateo de pieChart
        PieChartView pieChartView =  findViewById(R.id.chart);

        // ArrayList para añadir valores a pieChart
        List pieData = new ArrayList<>();

        // Se añaden valores a pieChart, valor, color y texto
        pieData.add(new SliceValue(45, Color.rgb(239, 149, 194 )).setLabel("Maybelline: 45%" ));
        pieData.add(new SliceValue(5, Color.rgb(255, 105, 180)).setLabel("Covergirl: 5%"));
        pieData.add(new SliceValue(25, Color.rgb(238, 130, 238  )).setLabel("Dior: 25%"));
        pieData.add(new SliceValue(15, Color.rgb(255, 182, 193 )).setLabel("NYX: 15%"));
        pieData.add(new SliceValue(10, Color.rgb(219, 112, 147 )).setLabel("Benefit: 10%"));

        // Instancia para valores generales
        PieChartData pieChartData = new PieChartData(pieData);
        pieChartData.setHasLabels(true).setValueLabelTextSize(14);
        pieChartData.setHasCenterCircle(true).setCenterText1("% de Ventas").setCenterText1FontSize(20).setCenterText1Color(Color.parseColor("#cd5d7d"));
        pieChartView.setPieChartData(pieChartData);

    }
}
