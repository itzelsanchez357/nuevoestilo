package com.example.nuevoestilo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MabellineService {
    // Se guarda el url de la api
    String API_ROUTE = "/api/v1/products.json?brand=maybelline";
    // Método de envio get y se le asigna la ruta, se declara la Clase Call con el nombre del metódo que consumirá esta acción
    @GET(API_ROUTE)
    Call<List<Maquillaje>> getPost();

    // Se guarda el url de la api
    String API_ROUTE_Cover = "/api/v1/products.json?brand=covergirl";
    // Método de envio get y se le asigna la ruta, se declara la Clase Call con el nombre del metódo que consumirá esta acción
    @GET(API_ROUTE_Cover)
    Call<List<Maquillaje>> getPostCoverGirl();

    // Se guarda el url de la api
    String API_ROUTE_dior = "/api/v1/products.json?brand=dior";
    // Método de envio get y se le asigna la ruta, se declara la Clase Call con el nombre del metódo que consumirá esta acción
    @GET(API_ROUTE_dior)
    Call<List<Maquillaje>> getPostDior();

    // Se guarda el url de la api
    String API_ROUTE_nyx = "/api/v1/products.json?brand=nyx";
    // Método de envio get y se le asigna la ruta, se declara la Clase Call con el nombre del metódo que consumirá esta acción
    @GET(API_ROUTE_nyx)
    Call<List<Maquillaje>> getPostNyx();

    // Se guarda el url de la api
    String API_ROUTE_benefit = "/api/v1/products.json?brand=benefit";
    // Método de envio get y se le asigna la ruta, se declara la Clase Call con el nombre del metódo que consumirá esta acción
    @GET(API_ROUTE_benefit)
    Call<List<Maquillaje>> getPostBenefit();
}
