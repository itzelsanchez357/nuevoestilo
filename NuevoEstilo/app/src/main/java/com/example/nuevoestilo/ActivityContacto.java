package com.example.nuevoestilo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActivityContacto extends AppCompatActivity {
    // Definicion de variable con número de código
    private final int PHONE_CALL_CODE = 100;
    private EditText EdtNumero;
    private Button btnNumero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacto);

        // Casteo de cuadro de texto y botón
        EdtNumero = (EditText) findViewById(R.id.EdtNumero);
        btnNumero = (Button) findViewById(R.id.btnNumero);

        // Evento al botón
        btnNumero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Se obtiene el valor que se escribio en el textView y se guarda en variable num
                String num = EdtNumero.getText().toString();

                // Comprueba que num haya sido llenado
                if(num !=null){
                    // Se obtiene la versión, si esta es mayor o actual llama al método para obtener permisos
                    if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE);
                    }else{
                        // De lo contrario, se llama al método de las veriones anteriores
                        versionesAnteriores(num);
                    }
                }
            }

            private void versionesAnteriores (String num){
                // Intent implícito para permitir hacer llamada con el número que se añadio
                Intent intentLlamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + num));
                if (verificarPermisos(Manifest.permission.CALL_PHONE)){
                    // Se verifican permisos, si se aceptan se inicia el intent
                    startActivity(intentLlamada);
                }else{
                    // De lo contrario muestra mensaje
                    Toast.makeText(ActivityContacto.this, "configura los permisos", Toast.LENGTH_SHORT).show();
                }
            }

        });
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PHONE_CALL_CODE:
                String permission = permissions[0];
                int result = grantResults[0];
                if(permission.equals(Manifest.permission.CALL_PHONE)) {

                    if (result == PackageManager.PERMISSION_GRANTED) {
                        // Obtener número ingresado
                        String phoneNumber = EdtNumero.getText().toString();

                        // Intent implícito para permitir hacer llamada con el número que se añadio
                        Intent llamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
                        // Se verifica permisos
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) return;
                        startActivity(llamada);
                    } else {
                        Toast.makeText(this, "No aceptaste el permiso", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private boolean verificarPermisos (String permiso){
        // Se verifican permisos
        int resultado = this.checkCallingOrSelfPermission(permiso);
        return resultado == PackageManager.PERMISSION_GRANTED;
    }
}