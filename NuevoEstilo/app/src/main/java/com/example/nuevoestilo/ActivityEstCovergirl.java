package com.example.nuevoestilo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.LineChartView;

public class ActivityEstCovergirl extends AppCompatActivity {
    LineChartView lineChartView;
    // Definición de array donde están los datos de la grafica del eje x
    String[] axisData = {"Id:486", "Id:349", "Id:284", "Id:465"};
    // Definición de array donde están los datos de la grafica del eje y
    int[] yAxisData = {20, 50, 50, 75};
    FloatingActionButton fBtnHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_est_covergirl);
        // Casteo de botón flotante (floatingActionButton)
        fBtnHome = (FloatingActionButton) findViewById(R.id.fBtnHome);
        // Dar evento a botón
        fBtnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent explícito que mostrará el ActivityBenefit
                Intent i = new Intent(v.getContext(), ActivityCovergirl.class);
                startActivity(i);
            }
        });

        // Casteo de chart
        lineChartView = findViewById(R.id.chart1);
        // Definición de array list para vaolores del eje x,y
        List yAxisValues = new ArrayList();
        List axisValues = new ArrayList();

        // Se asigna una linea a los valores de "y" y el color de la misma
        Line line = new Line(yAxisValues).setColor(Color.parseColor("#c86b85"));

        // Se recorre array de x para añadir valores al arrayList
        for (int i = 0; i < axisData.length; i++) {
            axisValues.add(i, new AxisValue(i).setLabel(axisData[i]));
        }
        // Se recorre array de y para añadir valores al arrayList
        for (int i = 0; i < yAxisData.length; i++) {
            yAxisValues.add(new PointValue(i, yAxisData[i]));
        }

        List lines = new ArrayList();
        lines.add(line);

        LineChartData data = new LineChartData();
        data.setLines(lines);
        // Propiedades para valores de x
        Axis axis = new Axis();
        axis.setValues(axisValues);
        axis.setTextSize(10);
        axis.setTextColor(Color.parseColor("#949cdf"));
        data.setAxisXBottom(axis);

        // Propiedades para valores de y
        Axis yAxis = new Axis();
        yAxis.setName("% de incremente en ventas");
        yAxis.setTextColor(Color.parseColor("#949cdf"));
        yAxis.setTextSize(12);
        data.setAxisYLeft(yAxis);

        lineChartView.setLineChartData(data);
        Viewport viewport = new Viewport(lineChartView.getMaximumViewport());
        viewport.top = 80;
        lineChartView.setMaximumViewport(viewport);
        lineChartView.setCurrentViewport(viewport);
    }
}