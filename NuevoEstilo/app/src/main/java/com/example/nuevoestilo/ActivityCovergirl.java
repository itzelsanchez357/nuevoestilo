package com.example.nuevoestilo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ActivityCovergirl extends AppCompatActivity {
    FloatingActionButton fBtnHome;
    FloatingActionButton fBtnEstadistica;
    TextView txt_id_m;
    TextView txt_brand_m;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_covergirl);
        // Casteo de botones flotantes (floatingActionButton) y cuadros de texto (textView)
        fBtnHome = (FloatingActionButton) findViewById(R.id.fBtnHome);
        fBtnEstadistica = (FloatingActionButton) findViewById(R.id.fBtnEstadistica);
        txt_id_m = (TextView) findViewById(R.id.txt_id_m);
        txt_brand_m = (TextView) findViewById(R.id.txt_brand_m);

        // Llamar a función para consumir API
        getPosts();

        // Dar evento a botón
        fBtnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent explícito que mostrará el ActivityCatalogo
                Intent i = new Intent(v.getContext(), ActivityCatalogo.class);
                // Iniciar activity
                startActivity(i);
            }
        });
        // Dar evento a botón
        fBtnEstadistica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent explícito que mostrará el ActivityEstCovergirl
                Intent i = new Intent(v.getContext(), ActivityEstCovergirl.class);
                // Iniciar activity
                startActivity(i);
            }
        });
    }
    private void getPosts() {
        // Instancia de retrofit donde se establece la URL base para consumir la API y  GJSON permite convertir objetos json a java
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://makeup-api.herokuapp.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Instacia de interface "MabellineService"
        MabellineService mabellineService = retrofit.create(MabellineService.class);

        // Se hace la llamada http implementando el método que se definio en interface MabellineService
        Call<List<Maquillaje>> call = mabellineService.getPostCoverGirl();

        // Método callback que tendra 2 opciones, uno si llega respuesta http y el segundo se hay alguna falla de conexión
        call.enqueue(new Callback<List<Maquillaje>>() {
            @Override
            public void onResponse(Call<List<Maquillaje>> call, Response<List<Maquillaje>> response) {
                // En caso que la respuesta no sea satisfactoria mostrar el código de error
                if (!response.isSuccessful()){
                    txt_id_m.setText("codigo:"+response.code());
                    return;
                }
                List<Maquillaje> postsList = response.body();
                // Se muestrán los elementos en base a la clase
                for(Maquillaje marca : response.body()) {
                    String c2="";

                    c2 += "Id: "+ marca.getId() +"\n\n"+  "Nombre: "+ marca.getName() +"\n\n"+"Precio: " +marca.getPrice()+ "\n\n"+"Link: "+ marca.getProduct_link() +"\n\n"+ "Descripcion: "+ marca.getDescription() +"\n\n\n";

                    txt_brand_m.append(c2);
                }
            }

            @Override
            public void onFailure(Call<List<Maquillaje>> call, Throwable t) {
                txt_id_m.setText(t.getMessage());
            }
        });

    }
}