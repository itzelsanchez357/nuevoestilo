package com.example.nuevoestilo;

public class Usuario {
    // Clase Usuario
    public String usuario;
    public String nombre;
    public String edad;
    public String telefono;
    public String descripcion;
    public String password;

    public Usuario(String usuario, String nombre, String edad, String telefono, String descripcion, String password) {
        this.usuario = usuario;
        this.nombre = nombre;
        this.edad = edad;
        this.telefono = telefono;
        this.descripcion = descripcion;
        this.password = password;
    }
    public Usuario(String nombre, String edad, String telefono, String descripcion) {
        this.nombre = nombre;
        this.edad = edad;
        this.telefono = telefono;
        this.descripcion = descripcion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
