package com.example.nuevoestilo;

import java.util.List;

public class Registro {
    // Clase Registro
    private List<Usuario> usuario;

    public Registro(List<Usuario> usuario) {
        this.usuario = usuario;
    }

    public List<Usuario> getUsuario() {
        return usuario;
    }

    public void setUsuario(List<Usuario> usuario) {
        this.usuario = usuario;
    }
}
