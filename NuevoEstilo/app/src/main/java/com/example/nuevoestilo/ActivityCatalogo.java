package com.example.nuevoestilo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ActivityCatalogo extends AppCompatActivity {
    FloatingActionButton fBtnHome;
    Button btn_ver_m1;
    Button btn_ver_m2;
    Button btn_ver_m3;
    Button btn_ver_m4;
    Button btn_ver_m5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalogo);
        // Casteo de botones flotantes (floatingActionButton) y botones
        fBtnHome = (FloatingActionButton) findViewById(R.id.fBtnHome);
        btn_ver_m1 = (Button) findViewById(R.id.btn_ver_m1);
        btn_ver_m5 = (Button) findViewById(R.id.btn_ver_m5);
        btn_ver_m2 = (Button) findViewById(R.id.btn_ver_m2);
        btn_ver_m3 = (Button) findViewById(R.id.btn_ver_m3);
        btn_ver_m4 = (Button) findViewById(R.id.btn_ver_m4);

        // Dar evento a botón
        fBtnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent explícito que  mostrará el ActivityMenu
                Intent i = new Intent(v.getContext(), ActivityMenu.class);
                // Iniciar activity
                startActivity(i);
            }
        });

        // Dar evento a botón
        btn_ver_m4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent explícito que mostrará el ActivityNYK
                Intent i = new Intent(v.getContext(), ActivityNYK.class);
                // Iniciar activity
                startActivity(i);
            }
        });

        btn_ver_m3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent explícito que mostrará el ActivityDior
                Intent i = new Intent(v.getContext(), ActivityDior.class);
                // Iniciar activity
                startActivity(i);
            }
        });

        btn_ver_m1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent explícito que mostrará el ActivityMaybelline
                Intent i = new Intent(v.getContext(), ActivityMaybelline.class);
                // Iniciar activity
                startActivity(i);
            }
        });
        btn_ver_m5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent explícito que mostrará el ActivityBenefit
                Intent i = new Intent(v.getContext(), ActivityBenefit.class);
                // Iniciar activity
                startActivity(i);
            }
        });
        btn_ver_m2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent explícito que mostrará el ActivityCovergirl
                Intent i = new Intent(v.getContext(), ActivityCovergirl.class);
                // Iniciar activity
                startActivity(i);
            }
        });
    }
}