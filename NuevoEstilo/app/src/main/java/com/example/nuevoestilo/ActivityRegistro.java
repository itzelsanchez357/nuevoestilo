package com.example.nuevoestilo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ActivityRegistro extends AppCompatActivity {
    // ArrayList Usuario
    List<Usuario> list = new ArrayList<Usuario>();
    Usuario usuario1;
    Registro registro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        // Casteo de botón
        Button btnAceptarRegistro = (Button) findViewById(R.id.btnAceptarRegistro);

        // Dar evento a botón
        btnAceptarRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Casteo de EditText
                EditText edtUsuarioRegistro = (EditText) findViewById(R.id.edtUsuarioRegistro);
                EditText edtNombreRegistro = (EditText) findViewById(R.id.edtNombreRegistro);
                EditText edtEdad_r = (EditText) findViewById(R.id.edtEdad_r);
                EditText edtTelefonoRegistro = (EditText) findViewById(R.id.edtTelefonoRegistro);
                EditText edtDescripcionR = (EditText) findViewById(R.id.edtDescripcionR);
                EditText edtPassword = (EditText) findViewById(R.id.edtPassword);

                // Se obtiene el valor de los EditText y se guardan en una variable
                String usuario = edtUsuarioRegistro.getText().toString();
                String nombre = edtNombreRegistro.getText().toString();
                String edad = edtEdad_r.getText().toString();
                String telefono = edtTelefonoRegistro.getText().toString();
                String descripcion = edtDescripcionR.getText().toString();
                String password = edtPassword.getText().toString();

                // Si los campos están vacios muestra mensaje de error
                if(usuario.isEmpty()||nombre.isEmpty()||edad.isEmpty()||telefono.isEmpty()||descripcion.isEmpty()||password.isEmpty()){
                    Toast.makeText(ActivityRegistro.this, "¡Error! Algún campo esta vacío", Toast.LENGTH_SHORT).show();
                }else{
                    // De lo contrario, añade al usuario
                    usuario1 = new Usuario(usuario,nombre,edad,telefono,descripcion,password);
                    list.add(usuario1);
                    registro = new Registro(list);

                    // Se crea intent explicito donde se le asigna el nombre i que llevara a LoginActivity
                    Intent i = new Intent(v.getContext(), ActivityLogin.class);
                    i.putExtra("usuario_registro_extra", usuario);
                    i.putExtra("nombre_registro_extra", nombre);
                    i.putExtra("password_registro_extra", password);
                    i.putExtra("edad_registro_extra", edad);
                    i.putExtra("telefono_registro_extra", telefono);
                    i.putExtra("descripcion_extra", descripcion);

                    // Inicia o se muestra el activity
                    startActivity(i);
                }
            }
        });

    }
}